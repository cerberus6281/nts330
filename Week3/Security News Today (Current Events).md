---                                                                             
layout: post
title:  Security News Today (Current Events)
date: 2019-03-26T22:55:48
author: cerberus
summary: >
  Briefly introduce pegasus and mention how it was used in a whatsapp breach.
categories: current events 
thumbnail: 
tags:
 - pegasus
 - whatsapp
 - breach
---

# Whats app breach
[whatsapp_breach](https://thehackernews.com/2019/05/hack-whatsapp-vulnerability.html)
## Brief Overview
The popular messaging app WhatsApp has been hit with a zero day exploit that allowed the hacker to install spyware on your device.
THe hackerswere able toinstall thespyware "Pegasus". THe zero day was found in whatsapp missed call fature. In this attack a hacker places a call to the victim 
even if the victim fails to answer the spyware is injected into the device. The spyware also deletes the message log to not raise suspicion. 
Pegasus can then proceed to steal all the information from the device and it can even turn on the mic and camera.
The Department of Justice had to be alerted of the vulnerability
### NSO group
THe NSO roup is a famous Israeli based hacking firm.
They are known for building some of the most advanced mobile hacking tools.
[NSO](https://www.ft.com/content/7f2f39b2-733e-11e9-bf5c-6eeb837566c5
)
THey own the powerful "Pegasus"spyware that is used to steal information from devices remotely using an exploit found in Whatsapp.

## Pegasus tool
[pegasus](https://thehackernews.com/2018/09/android-ios-hacking-tool.html)
### Brief overview
https://gitlab.com/cerberus6281/nts330/edit/master/Week3/Security%20News%20Today%20(Current%20Events).md#preview
## Conclusion
Even with the advanced engineers from Whatsappa security firm was able to develop a tool to hack them.
It goes to show that zero days are still being discovered and it is important to stay up to date.
THe NSo group focuses on developingtools that can remotely hack devicesfor intelligence agencies.
The Pegasus tool is a powerful spyware otol that can be used to steal critical information from the device.

## References
[whatsapp_hack](https://thehackernews.com/2019/05/hack-whatsapp-vulnerability.html)  
[NSO_group](https://www.ft.com/content/7f2f39b2-733e-11e9-bf5c-6eeb837566c5)  
[Pegasus_tool](https://thehackernews.com/2018/09/android-ios-hacking-tool.html)