# Nmap 7.70 scan initiated Mon Jun  3 01:35:54 2019 as: nmap -oN testsave4.txt 192.168.19.131
Nmap scan report for 192.168.19.131
Host is up (0.0017s latency).
Not shown: 994 closed ports
PORT      STATE SERVICE
22/tcp    open  ssh
80/tcp    open  http
111/tcp   open  rpcbind
139/tcp   open  netbios-ssn
443/tcp   open  https
32768/tcp open  filenet-tms
MAC Address: 00:0C:29:C2:3A:5E (VMware)

# Nmap done at Mon Jun  3 01:36:07 2019 -- 1 IP address (1 host up) scanned in 13.26 seconds
