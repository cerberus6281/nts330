---                                                                             
layout: post
title: Discussion-03: Recon
date: 2019-03-26T21:52:48
author: cerberus
summary: >
  conduct recon on bug bounty programs. include your likes and dislikes
categories: bounty 
thumbnail: 
tags:
 - bounty
 - bug bounty
 - recon
---

# first company
## Credit Karma
[CreditKarma](https://hackerone.com/creditkarma)
### Brief overview
This company focuses on providing users with free credit reports 
and they also show the current credit card rates and loan offers.

### Likes
* That they have a 100% resposne time which meand they respond on time
* they have paid more than 3 bounties and the highest paid was $2,250.
### Dislikes
* In the hacktivity section of hackerone.com there are several payouts but some do not have the amount


# Second company
## Ngnix
[Ngnix](https://hackerone.com/ibb-nginx)
### Brief Overview
Is a free and open source http server and proxy server. It is ideal for internet activity as it provides the web server that helps a web app function.
### Likes
* has a $3,000 payout for finding a bug has paid it out twice
* Ngnix is a very popular web server/proxy


### Dislikes

* THe bug bounty program lacks information it is very brief
companies like credit karmas bug bounty program is detailed better.
* company has not paid out in 5 years


## Conclusion

Bug bounty programs are interesting programs that I do not trust entirely as the bug bounty program may not be as advertised. 
The best way to find out if they are worthwhile is to read the fine print as most are explained in [hackerone.com](
https://hackerone.com/directory?order_direction=DESC&order_field=started_accepting_at+ "Directory").
This does not mean that bug bounty programs are not realiable as cmopanies want pen testers and 'hackers'
to hack their systems. THe reason I chose those companies was that they are popular and am somewhat familiar with both

## References
https://hackerone.com/directory?order_direction=DESC&order_field=started_accepting_at+
