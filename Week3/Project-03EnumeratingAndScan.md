---                                                                             
layout: post
title:  Project-03: EnumeratingAndScan 
date: 2019-05-26T22:55:48
author: cerberus
summary: >
Run port scanners(nmap) on a vulnhub vm from vulnhub.com 
  
categories: Projects 
thumbnail: 
tags:
 - port
 - scanner
 - nmap

---

# Introduction
The goal of this projecct is to run nmap on a known vuln vm.
THe vm will be downloaded from vulnhub.

## set Up
* download the vm from the following link [vulnhub](https://www.vulnhub.com/entry/kioptrix-level-1-1,22/)
* Kali VM
* ip range 192.168.19.128-254 /24 (Custom vnet adapter)




## Part 1

Once the vm has been set up run the following commands on kali  

```
netdiscover -i eth0
```  

this will scan our internal network to check which ips our interface is talking to:    
    
![netdisc](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/proj3/netDisc.PNG)      
    
From here we have found the vulnhub vm which is the 192.168.19.131  

**the victim has been identified**

### Scan a host adjusting the timing of requests with Nmap. 
```
nmap 192.168.19.131 -T4
```

### Use Nmap to sweep the scope for systems running web servers on port 80 and port 443. 
```
nmap -p80,443 192.168.19.131
```

![](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/proj3/portSc.PNG)  


### enables OS and version 
### detection script scanning and traceroute
```
nmap -A -T4 192.168.19.131

```

### Run a scan on a host and tell Nmap to display the reason it finds the port in the state it does. 

```
nmap -reason -T4 192.168.19.131
```
![](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/proj3/snipSV2.PNG)  

### Scan a system with Nmap and output the results to a Normal File.

```
nmap -oN testsave.txt 192.168.19.131
```
![](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/proj3/snipSv.PNG)  



Just provide the command you would use, you do not have to append the results or the file. 

### Scan a host as if it where denying ICMP (ping).
```
nmap -PS 192.168.19.131
```
    
    
### Port scan on a host for open ports 1 through 500 with Ncat. Yes, Ncat.
```
nc -v -z -w1 192.168.19.130 1-500
```

# When do you think you might use Ncat vs Nmap? 

Ncat is a handy tool that can create a chatroom if needed.
This protocol would be handy when you need to communicate with someone
or a group in a chatroom.
If you need to get send messages simple create a [chatroom](https://diablohorn.com/2011/07/21/quick-dirty-secure-chat-ncat/)  



## Perform Operating System identification on one of the hosts on your network.
![](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/proj3/osDetect.PNG)
        
### How accurate was the guess by the tool? 
Very accurate as the server is indeed running the services and the OS.  

## In your estimation did Nmap properly identify the services running on the machine?
Yes  

## Where there unknown application fingerprints? 
No  

# If Nmap doesn’t know what a service is, what steps could you take to determine what the service is? 
You are on a penetration test. Your customer asks you to identify all of the hosts in a given network range.
You notice that they are filtering ICMP so you can’t ping hosts to determine if they are alive. How would you determine which
hosts in the network range are actually up? 

When the Ping probe is disabled you can send some SYN packets by using this command:  
```
nmap -PS <hostname>
```  
Using this method it doesn't care if it returnsa packet it just simply wants to know if the host is up.  
THus this bypasses the Ping probe.  

## Take a couple of the hosts from your network and put them in a plain text file. 
## Put the IP addresses in the file so there is only one per line. Name this file “networkhosts.txt” 
## Use Nmap with the appropriate command line argument to import this file and scan the contents.
![](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/proj3/snipScanTxt.PNG)  

#Part 2

## Enter the IP address of the target system into OpenVAS and Scan it.  
![](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/proj3/newTarget.PNG)  

## Create a new custom scan policy in OpenVAS
![](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/proj3/newportList.PNG)  
* Give a few examples of checks that you removed. Give a few examples of checks that you kept.  

I kept the ports that I thought were relevant to the web server.  
```
21(ftp)
22(ssh)
80(http)
110(POP3)
443(https)
```  

* Do you believe that there are vulnerabilities on the system that the vulnerability scanner didn’t find? Why do you believe so?

No, since the system was vulnerable by desing the servicesthat are running on the server oare unpatched.

## Export the data from the scan in a format you can read later.
[scan_results](https://gitlab.com/cerberus6281/nts330/blob/master/screenshots/proj3/report-93f68859-72bc-494c-95bb-2848e7b24b74.pdf)  

## References
[vulnhub](https://www.vulnhub.com/entry/kioptrix-level-1-1,22/)  
[useful_nmap](https://securitytrails.com/blog/top-15-nmap-commands-to-scan-remote-hosts)

