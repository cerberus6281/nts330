---                                                                             
layout: post
title:  Project-01: Set-Up 
date: 2019-03-26T22:55:48
author: cerberus
summary: >
Will be conducting some recon on company backblaze.com 
  
categories: Projects 
thumbnail: 
tags:
 - set up

---
# Virtual Setup


## Introduction:
The purpose of this document is to set up a virtual environment. This way we can create a sandbox environment so we can test vulnerabilities separated from the production environment.
VMware will be used to run the virtual machines Kali Linux will be used as the attack machine, defending machines will be built at a later time. For now, we are trying to establish a virtual environment with a working virtual machine (VM).
The set-up will have the following:
VMware Workstation (application that runs virtual machines)  
![](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/Pro1/vmware.png)  
 


## ISO images (these images are used to install *OS)
 
![](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/Pro1/isotypes.png)  


## Configuration
Once the Iso image has been downloaded and stored somewhere we can remember. Now it is time to proceed with the vm on VMware.(Note: Vm was installed prior to start of document) the following is the configuration for the attack machine. The version of Kali is kali-linux-2019.1a-amd64.iso.
 

After going through the installation process of Kali we will then launch a terminal and run the following command:
```
sudo apt-get update && sudo apt-get upgrade
``` 

A total of 89 packages needed to be upgraded

There are times when packages will be held back due to package dependability issues so it is essential to run the following commands
```
sudo apt dist-upgrade  
```
 
 
And finally run 
```
sudo apt autoremove 
```
**to remove packages that are no longer required**

After the updates are installed we can now proceed to our next step of our test and that is gathering information.

## Vm information
Now we must find out some information regarding the VM. Running the following command will give us the information we need
Ifconfig will produce the following results 

![](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/Pro1/kaliNtinfo.png) 
 
### Since the VM is set up as NAT the ip address will be different from the router IP
    VM = 192.168.192.130
    Router = 10.0.1.1
 
# Gathering Intel 

Now in the attack box we will launch a terminal and begin to gather intel on our target **uat.edu**

By running:   
```
host uat.edu
```
it will produce the ip address associated with it  
![](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/Pro1/snipHost.png) 


### Using dig

Dig is used to show query displays the host ip address of the website,mail exchange, name servers

**Notes on dig:**
Gives header look for status: NOERROR this results with a successful query
Answer provides how many ip addresses will be returned
Will query A (IP address) by default
Question section
Query time 
MX servers are  mail servers by providing the MX command we can find out the mail server information
dig hostname MX (note: using www in front of the host will not return the mail server info must be host name minus www.)

dig hostname NS
will provide list of name servers on domain

Here is a snapshot of the script used:  

![](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/Pro1/scriptResults.png) 

## Conclusion
The virtual environment has been established some information has been gathered
on the target and information about the VM has been provided. 
This is only the beginning we were successful in creating the VM, 
getting ip info on host and VM, and gathering some intel on the host in question.



## References
[commands_sub](http://tldp.org/LDP/abs/html/commandsub.html)  
[How to Grep for Text in Files](https://www.linode.com/docs/tools-reference/tools/how-to-grep-for-text-in-files/)  
[How to update Kali Linux](https://linuxconfig.org/how-to-update-kali-linux)  

