---                                                                             
layout: post
title:  Project-02: Recon 
date: 2019-03-26T22:55:48
author: cerberus
summary: >
Will be conducting some recon on company backblaze.com 
  
categories: Projects 
thumbnail: 
tags:
 - backblaze
 - recon

---

# Introduction
> paragraph
## set Up

### steps

```
code blocks

```
######
code is used for images
![Minion](https://octodex.github.com/images/minion.png)

## Conclusion
WIth hacker groups like the SHadow Brokers I expect to see more NSA toll leaks or tool leaks in general.  


## References
links [MS17](https://github.com/rapid7/metasploit-framework/pull/9473)  
[CVE-details](https://www.cvedetails.com/cve/CVE-2017-0143/)  

##tables

| Task                     | Date     |Start Time| End Time | Comments |
| ------------------------ | -------- |----------|----------|----------|
| Run Shodan Scans         |5/25/2019 | 12:22    | 12:32    |Created account ran a few scans|
| Gather Results           |5/25/2019 | 13:00    | 13:25    |search for hostname, find ip web server, internet services,OS|
| Research Google Dorking  |5/25/2019 | 14:00    | on-going |site: filetype:pdf password |
| Add Api Keys to Recon-Ng |5/25/2019 | 14:35    | 14:40    |use command **keys add shodan_api** (had to look up how to do it) https://hackertarget.com/recon-ng-tutorial/ |
| Research Recon-Ng        |5/25/2019 | 15:00    | on-going |show modules, show info, issue with running some commands using shodan api need to upgrade to PRO version