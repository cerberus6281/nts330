# Log file for tasks				
				
| Task                        | Date     |Start Time| End Time | Comments |
| --------------------------- | -------- |----------|----------|----------|
| Download Kali ISo           |5/9/2019  | 10:22    | 10:35    |downloaded the iso kali-linux-2019.1a-amd64.iso|
| Install iso image on Vmware |5/9/2019  | 10:40    | 10:45    |Memory 6 GB RAM, Processors: 4 , NetAdap: NAT, Storage: 60GB|
| Update Kali                 |5/11/2019 | 11:49    | 11:55    |use apt update && apt full-upgrade 
| update Kali apt             |5/12/2019 | 21:44    | 21:46    |
| create script               |5/12/2019 | 22:10    | 22:23    |script will incorporate dig command, host