---                                                                             
layout: post
title:  FinalProject: HacktheBox 
date: 2019-06-07T22:15:20
author: cerberus
summary: 
Project will attempt to gain an invite code from hackthebox
categories: Projects 
thumbnail: 
tags:
 - hackthebox
 - curl
 - dev tools

---

# Final(Hack the Box):

In this final task the objective was to get into hackthebo by getting an invite code
and succesully gaining entry.  
Here are the steps I took to break the box:  
  
## Step 1
In this step I highlighted the text box right-clicked and selected inspect element
In the field I saw that the "type" was text which led me to attempt to put in special characters
into the text field with no success it kept returning the same results:  
![](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/week5/final/onessaa.PNG)  



## Step 2
In the next step I attempted to search for a XSS vulnerability by inputting this into the text field:
```
" onfocus="alert(1)" autofocus="
```

This yielded with the following results:
![](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/week5/final/onessa.PNG)

no entry.. yet


## Step 3

In this step I continued to look through the code and came uponsomething interesting 
there was a script with the following information: 
![](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/week5/final/onessb.PNG)  

here I noticed there was an invite api

I clicked on **Debugger** and then **Sources** from here I noticed there was a function called makeinviteCode.  
This prompted me to call the function in the console:  
  
```
makeinviteCode()  
```  

WIth this it yielded the following results:  
  
![](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/week5/final/onessc.PNG)  
  

and in the results I attempted to put the code into the text box and again no success  
  
After further evaluation I noticed that there was a enctype that read "ROT13" which made me google ROT13 decryptor  
  
I navigated to the following site: [dcode.fr](https://www.dcode.fr/rot-13-cipher)   
  
Once I supplied it with the code it yielded the following results:  
![](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/week5/final/onessd.PNG)  
  
Instructing to "make a POST request"  
  
Back to Google "how to make a post request using Linux"  

In Google I came across curl and how to get a post request from the terminal.    
thanks to [subfuzion](https://gist.github.com/subfuzion/08c5d85437d5d4f00e58)      
    
Next it was on to the terminal and I typed the following command  

```
curl -XPOST https://www.hackthebox.eu/api/invite/generate
```  
It returned the following:   
![](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/week5/final/onesse.PNG)  
  
WIth this I attempted to put the code in  
I had no success I tried to decrypt it by using ROT13 decryptor and still no success  

After doing more research priors who gained entry to the box were using base64 
The next step was to navigate to [base64decode.org](https://www.base64decode.org/)  
![](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/week5/final/onessf.PNG)  

This yielded the code:  
![](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/week5/final/onessg.PNG)  

**Back to hackthebox**  

In hack the box I entered the code and was finally successful.  

![](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/week5/final/onessh.PNG)  

# Conclusion

This assignment taught me so much and allowed me to put the skills I acquired from this course to work.  
Of course in the real world or a network with layered security wouldn't be as easy but
I learned that persistence takes you very far. Hacking is not the real term to define pen testers
A pen tester is an engineer with a multitude of skills. 




