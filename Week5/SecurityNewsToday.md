---                                                                             
layout: post
title:  Security News 
date: 2019-06-06T17:05:24
author: cerberus
summary: >
current events XSS scripting zero day found in social media buttons
categories: News 
thumbnail: 
tags:
 - XSS
 - zero day
 - social warfare

---

# Introduction
XSS or Cross site scripting is a very handy tool attackers use to gather information or to hijack web sessions  
A zero day was found in a wordpress social media sharing plugin.  
The pllugin in question is called Social warfare  
and what this plugin does it creates a social media bar that  
scrolls automatically to the bottom of the page  

![](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/week5/swwsasdf.PNG)  


image source [namehero.com](https://www.namehero.com/wordpress-hosting.php)

## This way you can instantly share the content on your social media page ##  

## Zero Day
A zero day was found within the debug features of the plugin and according to (Montpas,2019)
    "over 70,000 sites are using the Social Warfare plugin."
    
WIthin the debug featuresa an attacker can inject a mallicious script  
The attack in question is a Stored XSS meaning it will reside in the database
and will activate when a user clicks on the page. With this technique an attacker can take over
the browser environment. Leaving this unpatched is very dangerous.

## Conclusion
This XSS can be vry dangerousand with at least 70,000 sites using this plugin
it just opens up the attack surface
A patch has been released and it is imperative that they run the patch


## References
Montpas, M. A. (2019, March 21). Zero-Day Stored XSS in Social Warfare. Retrieved from [sucuri.net](https://blog.sucuri.net/2019/03/zero-day-stored-xss-in-social-warfare.html)  

WordPress Social Sharing Plugin – Social Warfare. (n.d.). Retrieved from [socia-warfare](https://wordpress.org/plugins/social-warfare/)  