---                                                                             
layout: post
title:  Project-05: NetworkSniffing 
date: 2019-06-07T13:22:35
author: cerberus
summary: 
Project will show some features of wireshark
categories: Projects 
thumbnail: 
tags:
 - wireshark
 - sniff
 - http

---

# Lab-05-Part-01 (Wireshark):

## Wireshark

Wireshark is a great tool that can sniff raw packets.
Meaning it will detect any data sent between the interfaces you choose
This way you are able to monitor each packet individually
It is helpful as it can monitor traffic and you can see who is talking to the interface and what packets they are sending 
Here is a great tutorial for wireshark [lifewire.com/wireshark-tutorial](https://www.lifewire.com/wireshark-tutorial-4143298) 


### Fire up Wireshark

#### After Wireshark loads go to; Capture > Interfaces 
Notice that you will see different network interfaces that Wireshark knows about.  
This could include Ethernet adapters, wireless adapters, loopback, and even virtual adapters.  
  
![](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/week5/proj5/captINt.PNG)  
  
**eth0 will be the interace used**  

    
### Choose the Options on your currently active network connection.  
  
![](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/week5/proj5/capoptsa.PNG)  
  
### Click on the Capture Filter button in this Capture Options dialog box. 
  
### Click on the filter: No ARP and no DNS. Do not click OK. Take a screenshot of the capture filter dialog when you select that filter. 
![](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/week5/proj5/wsFil.PNG)

### On the capture options dialog box, go ahead and select Start to start a capture.
![](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/week5/proj5/httpcap.PNG)

 
**Look through the traffic and see if you can find where your browser made a**    
**request to one of the web sites you visited. It should be HTTP and you might notice a GET or POST in the request as well.**  
**Right click on the individual packet and tell Wireshark to Follow TCP Stream.**  
  
![](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/week5/proj5/tcpsnip.PNG)  
  
  
## Use a display filter in Wireshark to have it filter out traffic that uses TCP traffic on port 80. Then click Apply to apply the filter.  
![](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/week5/proj5/dispport80.PNG)  
  
  
### example  

I was able to navigate to an http site [shpep.com](http://www.shpep.org/alumni-profiles/jacob-tice/)  
I found the jpg packet and was able to export the data packet to the desktop  
![](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/week5/proj5/httpcap.PNG)  
  
and since http is not encrypted I was able to show the image  
  
![](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/week5/proj5/imgdump.PNG)  
  
This is why it is best practice to use https so data can be enrypted.  
  
  
  
# Lab-05-Part-02 (PCAP):  

### EternalBlue packet analysis  

In this packet analysis of Eternal Blue there were some interesting bits of inforomation.  
  
![](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/week5/proj5/pktanalys.PNG)  

In packets 6 and 7  the SMB protocol is being used and is requesting a connection  

**packet 8 shows that the user anonymous has sent the request.**  

in packet 10 you can see the ip destination  
next to Path: \\192.168.198.203  

After it has established a $IPC connection or null connection( this feature allows users to send remote commands to a windows server under anonymous)  



#### About IPC$ share

    The IPC$ share is also known as a null session connection. 
    By using this session, Windows lets anonymous users perform certain activities, such as enumerating the names of domain accounts and network shares.
    The IPC$ share is created by the Windows Server service. 
    This special share exists to allow for subsequent named pipe connections to the server. 
    The server's named pipes are created by built-in operating system components and by any applications or services that are installed on the system. When the named pipe is being created,
    the process specifies the security that is associated with the pipe, and then makes sure that access is only granted to the specified users or groups.  

source [microsoft.com](https://support.microsoft.com/en-us/help/3034016/ipc-share-and-null-session-behavior-in-windows)  


With limited knowledge on wireshark reading packets takes practice and will plan to
research more on wireshark and further understanding packets
and how malware gets injected.  



## Lab-05 Part-03(Challenge):  

### What was the full URI of Vick Timmes' original web request? (Please include the port in your URI.)  
### Here filtering was helpful as it allwoed to determine the web request GET  
  
```
http.request.method == "GET"
```
  
![](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/week5/proj5/part3/ques1.PNG)  
  
    http://10.10.10.10:8080/index.php  
    the port used was 8080  
  
#### In response, the malicious web server sent back obfuscated JavaScript. Near the beginning of this code,  
#### the attacker created an array with 1300 elements labeled "COMMENT",  
#### then filled their data element with a string. What was the value of this string?  
![](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/week5/proj5/part3/ques2.PNG)  
  
    vEI  
  

### Vick's computer made a second HTTP request for an object.    


#### What was the filename of the object that was requested? 
  
while still having the same filter as step 1 yo uclick on file and export the object save it as raw  
![](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/week5/proj5/part3/ques7.PNG)  
  
    index.phpmfKSxSANkeTeNrah.gif  
  
#### What is the MD5sum of the object that was returned?  

open up a terminal run the following command to check the md5
    
```  
md5sum index.phpmfKSxSANkeTeNrah.gif  
```  
  
![](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/week5/proj5/part3/ques3.PNG)  
  
  
    df3e567d6f16d040326c7a0ea29a4f41  index.phpmfKSxSANkeTeNrah.gif  


#### When was the TCP session on port 4444 opened?  
(Provide the number of seconds since the beginning of the packet capture, rounded to tenths of a second. ie, 49.5 seconds)  
  
![](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/week5/proj5/part3/ques4.PNG)  
  
#### When was the TCP session on port 4444 closed?  
(Provide the number of seconds since the beginning of the packet capture, rounded to tenths of a second. ie, 49.5 seconds)  
![](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/week5/proj5/part3/ques5.PNG)  
  
#### In packet 17, the malicious server sent a file to the client.  
![](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/week5/proj5/part3/pk17.PNG)  
  
        What type of file was it? Choose one:  
           * Windows executable  
              
#### What was the MD5sum of the file?  
![](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/week5/proj5/part3/ques8.PNG)  
  
#### Vick's computer repeatedly tried to connect back to the malicious server on port 4445, even after the original connection on port 4444 was closed. With respect to these repeated failed connection attempts:

#### How often does the TCP initial sequence number (ISN) change? (Choose one.)  
          
![](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/week5/proj5/part3/ques9.PNG)  
  
    Every third packet  
           
![](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/week5/proj5/part3/ques12.PNG)  
  
        
#### How often does the IP ID change? (Choose one.)

            Every packet
    
           
#### How often does the source port change? (Choose one.)  
           
            Every 10-15 seconds  
           


#### Eventually, the malicious server responded and opened a new connection.   
#### When was the TCP connection on port 4445 first successfully completed? (Provide the number of seconds since the beginning of the packet capture, rounded to tenths of a second. ie, 49.5 seconds)
  
![](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/week5/proj5/part3/ques10.PNG)  
  
  
  
#### Subsequently, the malicious server sent an executable file to the client on port 4445. What was the MD5 sum of this executable file?  
![](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/week5/proj5/part3/ques13.PNG)  
  
  
#### When was the TCP connection on port 4445 closed? (Provide the number of seconds since the beginning of the packet capture, rounded to tenths of a second. ie, 49.5 seconds)  
![](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/week5/proj5/part3/ques11.PNG)  
  
  

## Conclusion

WIreshark is an essential tool for network admins as  
it provides us with packet sniffing  
The filtering really helps when you are trying to narrow down your search  
Further research is needed and I will continue to work with wireshark  

  
  
## References
[ann_aurora](https://www.youtube.com/watch?v=PLrivgbFSg0)  

[eternal_blue](https://www.hackers-arise.com/single-post/2018/11/30/Network-Forensics-Part-2-Packet-Level-Analysis-of-the-EternalBlue-Exploit)  

[eternal_blue_explained](https://www.virusbulletin.com/virusbulletin/2018/06/eternalblue-prominent-threat-actor-20172018/)  

