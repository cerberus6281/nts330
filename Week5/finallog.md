## Final log

## Log

| Task                                         | Date       |Start Time| End Time    | Comments                                                                   |
| ----------------------------------------     | --------   |----------|----------   |----------                                                                  |
| fire up wireshark                            |6/07/2019   | 13:22    | 13:25       |sniffed packets coming from an http site and dumped a raw packet into an image|
| research wireshark                           |6/07/2019   | 13:25    | on-going    |Use a few filters to get familiar with it ex. http.request.method == "GET"|
| download and inspect malware packet          |6/07/2019   | 14:30    | on-going    |downloaded packet and began inspectinga lot of information is still needed for review |
| begin Ann Aurora challenge                   |6/07/2019   | 17:00    | on-going    |Was introduced to tools like tcpdump and foremost (foremost is a tool that really drew my attention) Will continue to analyze packets from this challenge even though I answered all questions |
| begin hackthebox challenge                   |6/07/2019   | 22:30    | 23:40       | Attempted to put in special chars in the field(no success), XSS(no success), navigate through code find makeinviteCode API and call function makeinviteCode()  decrypt the string using ROT13 use cURL for a post request decode the string using base64 