## Log

| Task                        | Date     |Start Time| End Time | Comments |
| ------------------------    | -------- |----------|----------|----------|
| configure burpsuite         |6/02/2019 | 22:57    | 23:10    |configured burpsuite as local host and port 8080, set up new rule|
| run Burppsuite              |6/02/2019 | 23:12    | 23:40    |Started intercepting from backblaze.com and after webgoat was configured ran it to capture webgoat|
| configured webgoat          |6/03/2019 | 07:00    | 07:15    |I used the M25(latest version) as of the timestamp , had issues running the java --add-modules java.xml.bind -jar as it is no longer supported on Java 11 |
| http proxy lessons          |6/03/2019 | 08:00    | 08:45    |Started intercepting requests from webgoat |
| XSS lessons                 |6/03/2019 | 09:00    | on-going |Getting familiar with JAvascript this will be ongoing training |
| SQL lessons                 |6/03/2019 | 11:00    | on-going |Ran query commands to retrieve data from tables