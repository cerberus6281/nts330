---                                                                             
layout: post
title:  Project-04: WebAttack 
date: 2019-06-02T22:52:22
author: cerberus
summary: 
Project will go into Burpsuite and Webgoat to search for vulnerabilities in web servers
categories: Projects 
thumbnail: 
tags:
 - burpsuite
 - webgoat
 - web server

---

# Lab-04-Part-01 (BurpSuite):

BurpSuite is a great tool for listening on  web requests.
It allows us to set up a proxy so we can listen in on the traffic that is being sent by the user and the server.  
It also has a abundance of helpful features including but not limited to:
* using spiders
* Assigning rules to help scan the websites
* view HTML raw code

With Burpsuite you can configure any web browser to intercept the traffic into a proxy.  
Burpsuite has been set up on Kali linux 2019.1 with Firefox as the browser.  
![](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/week4/pro4/ffProx.PNG)




## create a new rule:  

![](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/week4/pro4/newruleBurp.PNG)


Now with the new rule in place sending requests is abit faster(at least on my machine)  

I noticed that after intercepting traffic I was served with the following items:  

![](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/week4/pro4/burpSNip.PNG)  

Here we can see that there are several javascript files that the server utilizes to funciton properly.  
After doing so more digging I found that the HTML code can be viewd as raw or as hex.  
Headers are also useful as they can help fingerprint the server.  
This snip provides some information found:

![](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/week4/pro4/snipRAW.PNG)

## Thoughts

With only having alimited amount of time with BurpSuite and proxies. 
This is a great learning experience and something that will be ongoing.
BUrpsuite offers so many tools and makes web testing that much easier.

# Lab-04-Part-02 (WebGoat):

### install webgoat on kali

```
wget -ndhttps://github.com/WebGoat/WebGoat/releases/download/v8.0.0.M25/webgoat-server-8.0.0.M25.jar
```

## Configure webgoat
to launch webgoat and choose the ports use th following command:  

```
java -jar webgoat-server-8.0.0.M25.jar --server.port=9090 --server.address=127.0.0.1
```
![](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/week4/pro4/snipWBGt.PNG)

ensure you are using Java 11 since the project has migrated over to Java 11

once you run the command the program will begin to launch:

after it has been launched yo ucan now navigate ove rto firefox or browser of choice and input the following into the url

```
localhost:9090/WebGoat
```
![](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/week4/pro4/webgoatsnipFF.PNG)

(WebGoat is case sensitive ensure you type it correctly and if you have burpsuite configured forward the request)  


### (note:java --add-modules java.xml.bind is no longer available in Java 11)


## HTTP proxy
HTTP proxy allows us to capture traffic and forward it to another port.  

It acts like a MITM where you can intercept requests and forward it to another port.  
If the protocol used is HTTP you can capture the plain text being transmitted as http is not encrypted. 
Burp suite made this task easier than I expected  

(update installed foxy proxy addon and made it easier to toggle the proxy from firefoxrather than burp suite)

### here was able to capture the magic number
![](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/week4/pro4/wgt2.PNG)  


## THis was just a simple task that reversed the name entered

![](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/week4/pro4/wgL1.PNG)



## SQL
for SQL injection you can use the following tochain commands
from example the ' allows for string concatination and ; allows for query chaining
weclose out the queyrywith ; and ask for comments using the --  

    '; select * from user_system_data;--
    
![](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/week4/pro4/wgtadv1snip.PNG)    


### Running queries
SQL has a feature that allows you to query from tables  
This way you are able to retrieve data fast.  
HOwever when the table in question has not been sanitized this opens the table or the database
to an SQL injection.  



## update query
![](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/week4/pro4/dmlsnip.PNG)

One way to achieve this is to query special chracters.

![](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/week4/pro4/snipsqlls1.PNG)



## XSS
for cross site scripting this technique  
site displays content that is unsanitized
* has ability to inject malicious scripts into browser
* needs tohave access to directory where multiple users interact with web server(public)

## example 
**the credit card field is set to text where it should clearly be an int value.**  

![](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/week4/pro4/namefieldSNIP.PNG)

## Conclusion
Kali llinuxwas giving me a few problems after I created a payload(had to install a fresh copy)  
BUrp suite is a great tool for capturing data I was able to navigate through those exercises.
just had a few issues with setting up the ports(solved)  
Both tools when ran in unisongive give great results as burpsuite acts as the proxy and thus captures all the traffic.  
I was able to capture trafficfrom the recon assignment [backblaze.com](https://www.backblaze.com/)
and was also able to capture traffic from WebGoat. 
Webgoat is a app that I will continue to study on
to be continued...

## References
[webgoat](https://github.com/WebGoat)


## Log

| Task                        | Date     |Start Time| End Time | Comments |
| ------------------------    | -------- |----------|----------|----------|
| configure burpsuite         |6/02/2019 | 22:57    | 23:10    |configured burpsuite as local host and port 8080, set up new rule|
| run Burppsuite              |6/02/2019 | 23:12    | 23:40    |Started intercepting from backblaze.com and after webgoat was configured ran it to capture webgoat|
| configured webgoat          |6/03/2019 | 07:00    | 07:15    |I used the M25(latest version) as of the timestamp , had issues running the java --add-modules java.xml.bind -jar as it is no longer supported on Java 11 |
| http proxy lessons          |6/03/2019 | 08:00    | 08:45    |Started intercepting requests from webgoat |
| XSS lessons                 |6/03/2019 | 09:00    | on-going |Getting familiar with JAvascript this will be ongoing training |
| SQL lessons                 |6/03/2019 | 11:00    | on-going |Ran query commands to retrieve data from tables