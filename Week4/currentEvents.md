---                                                                             
layout: post
title:  SecurityNews 
date: 2019-06-02T17:53:32
author: cerberus
summary: >
discussing current events in Network security
and ransomware
categories: Projects 
thumbnail: 
tags:
 - current events
 - Dharma
 - ransomware

---

# Introduction
WIth ransomware on the rise it is up to security professionals to infrom the everyday users.  
Accroding to Phoenixnap.com "A total of 850.97 million ransomware infections were detected by the institute in 2018"(Dobran, 2019).  
It goes to show that malware is not going anywhere and is something that is affecting home users and orginazations.  
The lates attack on Baltimore has caused panic through out the city and the city has estimatedit will cost them $18.2 milliondollars to recover from (Duncan, 2019).

# Dharma

Dharma is a ransomware that is also considered a trojan as it is pushed out in phishing emails.    
THe mnalware claims that your system has been corrupted and prompts you to download the attachment.  
![](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/week4/dharmaSNap.PNG)  

Once the user launches the executable a friendly GUI with the legitimate softwarte runs.  
MEanwhile, in the background the malware is encrypting your files.  
Since the malware and the ESET run on differenct process the user will not be able to determine that the files are being encrypted.  
While you are distracted clicking through the installation process your files are being encrypted without the users knowledge.

# ways to protect yourself
Since this attack is done through phishing here azre some ways you can protect yourself:
* do not download suspicious attachments
* keep your OS system patched and up to date
* Ensure you are doing backups
* when in doubt do not open these links
* If you are curios consider opening it in a VM with no internet access.  * 

## Conclusion
Ransomware is a ongoing problem and Cities like Baltimore and Atlanta have felt the impact of this malware.  
Attacks like these are phishing attempts that certain users will fall bait to.  
It is up to us to get the word out and train the average user on how to help spot these things.


## References
Dobran, B. (2019, January 31). 27 Shocking Ransomware Statistics That Every IT Pro Needs To Know. Retrieved from https://phoenixnap.com/blog/ransomware-statistics-facts

Dubey, Y. (2019, May 9). Dharma Ransomware Installs Antivirus On PC Only To Encrypt Files Later. Retrieved from https://fossbytes.com/dharma-ransomware-intalls-antivirus-encrypts-file/

Duncan, I. (2019, June 1). Baltimore estimates cost of ransomware attack at $18.2 million as government begins to restore email accounts. Retrieved from https://www.baltimoresun.com/maryland/baltimore-city/bs-md-ci-ransomware-email-20190529-story.html
