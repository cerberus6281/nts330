---                                                                             
layout: post
title:  Project-02: Recon 
date: 2019-03-26T22:55:48
author: cerberus
summary: >
Will be conducting some recon on company backblaze.com 
  
categories: Projects 
thumbnail: 
tags:
 - backblaze
 - recon

---

# Introduction
Recon is key to a successful attack. This can be seen everywhere, the military uses it, contractors use it, and even when you go on vacation. 
Because you must know the area you will be navigating yourself around.
This helps eliminate the element of surprise. 
Recon also applies to a penetration tester.

## Target

The target I chose to conduct research on was [backblaze](https://www.backblaze.com).  
This company focuses on storage.
The company launched in 2007 and is based in the United States.
They offer competitive prices for cloud storage and back up your computer automatically. 

**here is a great resource for more information on the company. [cloudwards_review](https://www.cloudwards.net/review/backblaze/)**
 

### Here are some Strengths & Weaknesses

Strengths:

    Unlimited backup
    Low cost
    Easy to use
    Backup by file type
    Block-level backup
    Bandwidth speed up
    External hard drive backup
    Mobile file access
    Sharing capabilities
    Courier recovery
    File versioning
    Strong security features
    Live chat support

Weaknesses:

    Private encryption not end-to-end
    Limited to one computer
    No mobile backup
    Versioning limited to 30 days

  

## set up
These are the following items that will be used for recon:
* [Shodan.io](https://www.shodan.io/)
* Kali Linux
* Google

# Steps
## Step 1 Gather intel  
    
    
## Snip of Shodan scan
![Snip_of_Scan](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/shos.PNG)

>What web server are they using (Apache, IIS, etc.)? What version is it?

  * Apache/2.4.25
    
>    Does it appear they are hosting their own web server?

* Yes as the hostname points to 162.244.57.11.rdns.backblaze.com

>What programming languages are used on the site? 

* jQuery(JavaScript)
* HTML
* CSS
    
>What are the networks in use by the organization? List Ranges? 
* 162.244.58.9
* 162.244.58.46
* 104.20.245.63
* 104.153.232.252
* 162.244.57.11(moved permanently)
* 162.244.58.46
* 206.190.215.236
* 104.153.232.253(moved permanently)




    
> Does it appear they are hosting any other services from their network ranges? (Do Not Scan network segments) 

* They are using Cloud Flare DNS for their resolver
    SSL cert is being issued by www.digicert.com 
    
>Information using search engines: Google

    homepage
    https://www.backblaze.com 

    pricing for cloud storage
    https://www.backblaze.com/backup-pricing.html 

    twitter page
    https://twitter.com/backblaze?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor
    
  >Identify key employees.  Get names, positions, salary, phone #, and e-mail addresses.

```
site:https://www.backblaze.com 'ceo' returned https://www.backblaze.com/company/team.html

```

**Here are some key employees**

|Position | Name     | *Salary       |  
|:-----------------  |:--------------:|  
| CEO  |Gleb Budman  |$245,000       |         
| CTO  |Brian Wilson |$226,582       |  
| CFO  |John Tran    |$168,975       |  


*salaries based on [payscale.com](https://www.payscale.com/research/US/Country=United_States/Salary)
  
Do they participate in any professional organizations?
* Apple Consultants
* JFE Network
 
> Do they participate in any professional social media sites? 
* LinkedIn

        Is anyone looking for a job?
No, not at this time

        Can you locate interesting corporate documentation, passwords, etc...? 
No, running dork commands no password files or files of interest returned.

  
      Does your target company have any associations with other companies? e.g.partners
They have parnered with SourceNext  
from [backblaze.com](https://www.backblaze.com/blog/konichiwa-backblaze-partners-with-japans-largest-software-vendor-sourcenext/)
> "The service provided in Japan still provides automatic backup,
unlimited storage, unthrottled bandwidth, 
robust encryption, restore via download or USB drive, 
Locate My Computer, and more for one flat rate. Additionally, the entire service is localized into Japanese."

## Create a visual map of your selected target's discovered systems.  Identify network address ranges, possible target systems and their purpose, routers, switches, etc......  Is this their DMZ?

**Search results in shodan.io only returned the following:**
![Snip_of_Scan](https://gitlab.com/cerberus6281/nts330/raw/master/screenshots/shoSnip.jpg)
  
   
        ```
        site:backblaze.com filetype:pdf password 
        site:backblaze.com filetype:xls password
        ```
    
    Step 3
    
        Using Recon-NG perform a full recon on your target company.  Document your results.  Did you find any additional useful or interesting info.
    Rules of Engagement
    DO NOT SCAN

## Conclusion

    Is there anything that you found particularly useful or juicy during your second phase of your information gathering exercise?
I will consider upgrading to  the Shodan PRO version. SHodan was very helpful in finding intel on the company.
Reconhelpsmake your job easier.
Although, it takes the longest it is an essential tool to have.
Please provide feedback and provide corrections where needed.


    
    What tools and web sites did you use during this lab exercise?
* GoogleDorking
* Shodan
* Kali Linux
* LinkedIn
* Payscale

## References
[backblaze](https://www.backblaze.com)  
[cloudwards_review](https://www.cloudwards.net/review/backblaze/)